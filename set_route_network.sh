#!/bin/bash
# Set route to network
iface=$(ip route show | grep default | awk '{print $5}')
ip route replace 144.91.64.0/18 via 144.91.64.1 dev $iface
# Fix for default route
sleep 10
ip route add default via 144.91.64.1
