#!/bin/sh

CHROOTS=(default-x86_64 gremlins-x86_64 goblins-x86_64)
USER=$SUDO_USER

for CHROOT in ${CHROOTS[@]}; do
  umount -R /var/lib/artools/buildpkg/${CHROOT}/root/dev
  umount -R /var/lib/artools/buildpkg/${CHROOT}/root
  umount -R /var/lib/artools/buildpkg/${CHROOT}/$USER
  rm -rf /var/lib/artools/buildpkg/${CHROOT}/$USER/build/*
done
