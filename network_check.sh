:

if ping -c1 1.1.1.1 -W 2 2>&1 >/dev/null; then
    true
else
    echo "*** NETWORK DOWN! Restarting net.eth0." >> /var/log/errors.log
    pkill -9 dhcpcd
    sleep 1
    rc-service net.eth0 restart
fi
